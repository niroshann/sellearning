package basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Lec36HandlingRadioButtonsWithCustomizedXpath {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "/home/niro/workspace/ThridPartyDrivers/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("http://www.echoecho.com/htmlforms10.htm");
		
		// checkout the difference between single and multiple element.
		driver.findElement(By.xpath("//input[@value='Butter']")).click();
		System.out.println(
							driver.findElements(By.xpath("//input[@name='group1']")).size()
						   );

	}

}
