package basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Lec43WebElementValidationPart_2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		WebDriver driver = new FirefoxDriver();
		driver.get("http://www.makemytrip.com/flights");
	
		/* Lec42 - Part-1
		System.out.println("Before user click on multicity/stop over radio button");
		System.out.println(driver.findElement(By.xpath("//a[@id='return_date_sec']")).isDisplayed());
		
		String text = driver.findElement(By.cssSelector("a[id='multi_city_button']")).getText();
		
		if (text.equals("MULTI CITY / STOP OVER")) {
			
			driver.findElement(By.xpath(".//*[@id='multi_city_button']/span")).click();
		}
		
		System.out.println(driver.findElement(By.xpath("//a[@id='return_date_sec']")).isDisplayed());
		*/
		
		// In case of validating any object present in the web page or code base use size();
				int count = driver.findElements(By.xpath("//span[@class='radio_state']")).size();
				
				if (count == 0){
					System.out.println("there aren't any radio buttons");
				} else {
					System.out.println("there are " + count + " radio buttons available");
				}	

	}

}
