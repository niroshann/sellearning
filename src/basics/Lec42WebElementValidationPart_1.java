package basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Lec42WebElementValidationPart_1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		WebDriver driver = new FirefoxDriver();
		driver.get("http://www.makemytrip.com/flights");
		
		System.out.println("Before user click on multicity/stop over radio button");
		
	// is.Displayed() only for the hidden elements which live in the page, its not for the element which is hidden and doesn't live in the page	
		System.out.println(driver.findElement(By.xpath("//a[@id='return_date_sec']")).isDisplayed());
		
		String text = driver.findElement(By.cssSelector("a[id='multi_city_button']")).getText();
		
		if (text.equals("MULTI CITY / STOP OVER")) {
			
			driver.findElement(By.xpath(".//*[@id='multi_city_button']/span")).click();
		}
		
		System.out.println(driver.findElement(By.xpath("//a[@id='return_date_sec']")).isDisplayed());


	}

}
