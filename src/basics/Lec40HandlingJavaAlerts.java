package basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Lec40HandlingJavaAlerts {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		
		driver.get("http://www.tizag.com/javascriptT/javascriptalert.php");
		
		driver.findElement(By.xpath("html/body/table[3]/tbody/tr[1]/td[2]/table/tbody/tr/td/div[4]/form/input")).click();
	
	// .getText() is to get the text from alert box
		System.out.println(driver.switchTo().alert().getText());
		
	// Accept() can be used for positive action buttons in alerts (ok/done/yes)
		driver.switchTo().alert().accept(); 
	
	// dismiss() can be used for (any negative action buttons in alerts "cancel","no","nay")
		//driver.switchTo().alert().dismiss();
		
	// sendKeys() to send keys into the emlement in the alert box	
		//driver.switchTo().alert().sendKeys("sending keys");
	}

}
