package basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Locator1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		WebDriver driver = new FirefoxDriver();
		//driver.get("http://qaclickacademy.com");
		//driver.findElement(By.xpath("//*[@id='homepage']/section[2]/div/a")).click();
		
		driver.get("http://www.facebook.com");
		driver.findElement(By.id("email")).sendKeys("test@test.com");
		//driver.findElement(By.name("pass")).sendKeys("testpassword");
		//driver.findElement(By.linkText("Forgotten your password?")).click();
		//driver.findElement(By.partialLinkText("Forgotten your")).click();
		
		driver.findElement(By.xpath("//*[@id='u_0_m']")).click();
		
	}

}
