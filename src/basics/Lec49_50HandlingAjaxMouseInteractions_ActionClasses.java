package basics;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class Lec49_50HandlingAjaxMouseInteractions_ActionClasses {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		
		driver.get("http://www.amazon.co.uk");
		
	/**
	 *  we pass in 'driver' as argument for Action() object,
	 *  now the object 'abc' will be able to use methods from both 'Actions' and 'driver' class
	 */
		Actions abc = new Actions(driver);
		
//		WebElement signinDropdown = driver.findElement(By.xpath("//*[@id='nav-link-yourAccount']"));
//		abc.moveToElement(signinDropdown).build().perform();
//		
		WebElement searchBox = driver.findElement(By.xpath("//input[@name='field-keywords']"));
		abc.keyDown(Keys.SHIFT).moveToElement(searchBox).click().sendKeys("nikon d").build().perform();
		abc.click(searchBox).contextClick(searchBox).build().perform();
		
//		WebElement searchKey = driver.findElement(By.xpath(".//*[@id='nav-search']/form/div[2]/div/input"));
//		abc.click(searchKey).build().perform();
		
	}

}
